# importing modules
from tkinter import *
from pytube import YouTube
import os

# creating the window
root = Tk()
root.title("Youtube Video Downloader")
root.minsize(width=500, height=300)
root.maxsize(width=500, height=300)
icon = PhotoImage(file="img/youtube-video-downloader-logo.png")
root.tk.call('wm', 'iconphoto', root._w, icon)

# entering the link
link = StringVar()
Label(root, text = 'Paste the youtube video link:', font = 'arial 18 bold').place(x= 80 , y = 65)
link_enter = Entry(root, width = 70,textvariable = link).place(x = 32, y = 110)

# path
save_path = "Videos"

def open():
    os.startfile(save_path, 'open')

# function for download the video
def Downloader():
    url =YouTube(str(link.get()))
    video = url.streams.first()
    video.download(save_path)
    Label(root, text = 'Downloaded successfully!! Developed By Avishek...', font = 'arial 13 bold', command = open()).place(x = 32 , y = 200)

Button(root,text = 'Click to download', font = 'arial 14 bold' ,bg = '#6E7AF9', fg = 'white', padx = 2, command = Downloader).place(x=150 ,y = 145)

root.mainloop()
